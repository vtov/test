#!/bin/bash

CI_COMMIT_REF_SLUG=$1
CI_PROJECT_NAME=$2


GREEN='\033[0;32m'
NC='\033[0m'

usage() {
    echo "Usage: $0 CI_COMMIT_REF_SLUG CI_PROJECT_NAME"
    echo "or: $0 branch_name project_name" 
}

base64_decode_key() {
if [[ "$OSTYPE" == "linux"* ]]
then
    echo "-d"

elif [[ "$OSTYPE" == "darwin"* ]]
then
    echo "-D"

else
    echo "--help"
fi
}


if [ -n "$CI_COMMIT_REF_SLUG" ] && [ -n "$CI_PROJECT_NAME" ]
then
    echo -e "${GREEN}creating namespace for project${NC}"
    kubectl create namespace \
        $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME

    echo
    echo -e "${GREEN}creating CI serviceaccount for project${NC}"
    kubectl create serviceaccount \
        --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
        $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME

    echo
    echo -e "${GREEN}creating CI role for project${NC}"
    cat << EOF | kubectl create --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME -f -
        apiVersion: rbac.authorization.k8s.io/v1
        kind: Role
        metadata:
          name: $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME
        rules:
        - apiGroups: ["", "extensions", "apps", "batch", "events", "certmanager.k8s.io", "cert-manager.io", "monitoring.coreos.com"]
          resources: ["*"]
          verbs: ["*"]
EOF

    echo
    echo -e "${GREEN}creating CI rolebinding for project${NC}"
    kubectl create rolebinding \
        --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
        --serviceaccount $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME:$CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
        --role $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
        $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME

    echo
    echo -e "${GREEN}access token for new CI user:${NC}"
    kubectl get secret \
        --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
        $( \
            kubectl get serviceaccount \
                --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
                $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME \
                -o jsonpath='{.secrets[].name}'\
        ) \
        -o jsonpath='{.data.token}' | base64 $(base64_decode_key)
    echo


    kubectl create secret docker-registry gitlab-testapp-registry \
            --docker-server registry.gitlab.com --docker-email 'ВАШ E-MAIL' \
            --docker-username 'ИМЯ ПОЛЬЗОВАТЕЛЯ ТОКЕНА' \
            --docker-password 'ПАРОЛЬ' \
            --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME




else
    usage
fi
