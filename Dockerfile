ARG IMAGE=nginx:1.19-alpine
FROM ${IMAGE}

ENV WEB_PORT 8080
ENV GRP=appgroup
ENV USR=appuser

RUN addgroup -S ${GRP} \
    && adduser -S ${USR} -G ${GRP}

RUN chown -R ${USR}:${GRP} /var/cache/nginx && \
        chown -R ${USR}:${GRP} /var/log/nginx && \
        chown -R ${USR}:${GRP} /etc/nginx/conf.d


COPY ./nginx_conf/nginx.conf /etc/nginx/
COPY ./nginx_conf/default.conf /etc/nginx/conf.d/
COPY ./app/index.html /usr/share/nginx/html/

WORKDIR /usr/share/nginx/html/

USER ${USR}

EXPOSE ${WEB_PORT}

CMD ["nginx", "-g", "daemon off;"]