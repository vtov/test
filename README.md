# Тестовое задание. Задание * (со звездочкой) Docker контейнер с микросервисом (nginx), pipeline и helm чартом для деплоя в кластер k8s.
 #### В docker контейнере запускается nginx под непривилегированным пользователем.
 #### Настроен pipeline со следующими стадиями:
   ##### build
        Из dockerfile собирается образ.  
        Условия: 
        - runner, которому будет выдано задание на сборку должен иметь тег test и executor shell
   ##### test
        Собранный на стадии build образ запускается и выполняется запрос к главной странице приложения.  
        Условия: 
        - runner с тегом test из стадии build
   ##### push
        Собранный на стадии build образ пушится в gitlab container registry.   
        Условия:   
        - runner с тегом test из стадии build
   ##### cleanup
        Запущенный на стадии test котейнер выключается, а образ, из которого он был запущен, удаляется.  
        Условия:  
        - runner с тегом test из стадии build
   ##### deploy 
        Контейнер, помещенный в gitlab container registry деплоится в клстер k8s
        Условия:   
        - раннер с executor docker.  
        - необходим сгенерированный deploy token для доступа в  gitlab container registry.  
        - необходим созданный для каждой ветки репозитория namespace с servieAccount и rolebinding для работы helm.

#### Управление стратегией rollingUpdate деплоя подов в deployment предусмотрено через переменные в .gitlab-ci.yml
        Вот эти переменные.   
```     
        --set maxSurge=0  
        --set maxUnavailable=1  
```
        Такая конфигурация соответствует поведению, описанному в пункте "i" тестового задания.  
```
        --set maxSurge=1  
        --set maxUnavailable=0  
```
        Такая конфигурация соответствует поведению, описанному в пункте "ii" тестового задания.  
        Через переменную --set host=$CI_COMMIT_REF_SLUG.$CI_PROJECT_NAME.skblab.ru можно задать server_name,  
        по котрому будет отвечать ingres c приложением из конкретной ветки. (в данном примере, предположим,  
        что домен третьего уровня backend-test в задании это имя проекта и оно подставится из переменной $CI_PROJECT_NAME) 
        Если не нужно управление этими переменными из .gitlab-ci.yml,   
        тогда их можно удалить, а значения переменных будут подставляться из файла values HELM чарта.  
		Также в .gitlab-ci.yml нужно указать ip адрес и порт вашего k8s кластера   K8S_API_URL: https://IP_АДРЕС_КЛАСТЕРА_k8s:6443

####  Подготовка кластера k8s.
      Перед запуском пайплайна необходимо подготовить кластер k8s. Это можно сделать при помощи скрипта setup.sh,  
      который должен быть запущен на узле, с установленным kubectl  
      и правами подключения в кластер под привелегированным serviceAccount.   
      Перед запуском скрипта setup.sh, нужно немного подготовиться.   
      Предварительно должен быть сгенерирован токен для доступа к  
      container registry (в меню прокета Settings - Repository - Deploy tokens),  
      при генерации токена, нужно установить чекбокс read_registry. Токен подключается в скрипте в секции  
```
            kubectl create secret docker-registry gitlab-testapp-registry \
            --docker-server registry.gitlab.com --docker-email 'ВАШ E-MAIL' \
            --docker-username 'ИМЯ ПОЛЬЗОВАТЕЛЯ ТОКЕНА' \
            --docker-password 'ПАРОЛЬ' \
            --namespace $CI_COMMIT_REF_SLUG-$CI_PROJECT_NAME
```

     Далее, скрипт запускается с параметрами  setup.sh CI_COMMIT_REF_SLUG CI_PROJECT_NAME  
      CI_COMMIT_REF_SLUG - имя ветки  
      CI_PROJECT_NAME - имя проекта  
      В результате бует созданы nnamespace с именем CI_COMMIT_REF_SLUG-CI_PROJECT_NAME,  
      необходимые serviceAccount, role, roleBinding.  
      А в консоль будет выдан токен, полученный из секрета созданного  
      serviceAccount который необходимо сохранить в   
      gitlab (в меню прокета Settings - CI/CD - Variables), чекбоксы Protect и Mask ставить не нужно.   
      А имя созданной переменной вставляется в .gitlab-ci.yml  
      в строке - kubectl config set-credentials ci --token=$ИМЯ_ПЕРЕМЕННОЙ.  

      
#### После выполнения этих манипуляций можно запускать пайплайн. Запуская скрипт для каждой для каждой ветки, получая токен пользователя кластера и подключая его в gilab  и прописывая имя переменной, содержащей токен в .gitlab-ci.yml, получаем поведение, описанное в пункте co * (звездочкой) тестового задания.
И немного пруфов, что это работает  
https://yadi.sk/i/C7TZbMVylOACzQ  
https://yadi.sk/i/9kfoB0lWjppPLA  
https://yadi.sk/i/sxVViH6cJKSAbw  

# Задание **(с двумя звездочками)
Для автоматического удаления стендов установим в кластер k8s утилиту kube-janitor. https://codeberg.org/hjacobs/kube-janitor
В манифесте rules.yaml уазываем то, что нам необходимо удалить и через сколько. В нашем случае, это стенд,  
развернутый в собственном неймспейсе task-123-test2, который мы будем удалять.   
Предположим, что действует стандарт именования стендов (а следовательно и неймспейсов) по шаблону task-*,  
тогда настроим удаление всех неймспейсов старше 24 часов, попадающх под этот шаблон.
```
- id: temporary-task-namespaces
  resources:
    - namespaces
  jmespath: "starts_with(metadata.name, 'task-')"
  ttl: 24h
```
